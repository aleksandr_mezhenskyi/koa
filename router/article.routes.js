const Router = require('koa-router')
const body = require('koa-body')
const articleController = require('../controllers/article.controller')
// const

const router = new Router({
    prefix: "/api"
})

router
    .get('/article/:alias', articleController.getByAlias)
    .get('/articles', articleController.getAll)
    .put('/article/:id', (ctx, next) => {

    })

    .del('/article/:id', (ctx, next) => {

    })
    .post('/article', body(), articleController.create)

module.exports = (app) => app.use(router.routes())