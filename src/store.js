import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    errors: [],
    users : new Map()
  },
  getters:{
    hasError: (state)=>(name)=>{
      let error = state.errors.find(e=> e[name]);
      if(!error) return;
      return error[name];
    },


  },
  mutations: {
    ADD_ERROR(state, error){
      let inputName = Object.keys(error)[0]; 
      
      let index = state.errors.findIndex(e=>{
        return e.hasOwnProperty(inputName)
      });

      if(index > -1) state.errors.splice(index, 1, error);
      else state.errors.push(error);
    },

    ADD_USER(state, user){
      state.users.set(user);
    },

    DELETE_USER(state, user){
      state.users.delete(user);
    }
  },
  actions: {

  }
})
