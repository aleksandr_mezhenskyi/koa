export function render(tag, options, childrens) {
    const el = document.createElement(tag);

    if (typeof options === 'object') {

        if (options.hasOwnProperty('attrs')) {
            for (let key in options.attrs) {
                el.setAttribute(key, options.attrs[key]);
            }
        }

        if (options.hasOwnProperty('style')) {
            for (let key in options.style) {
                el.style[key] = options.style[key];
            }
        }

        if (options.hasOwnProperty('dataset')) {
            for (let key in options.dataset) {
                el.dataset[key] = options.dataset[key];
            }
        }


        if (options.hasOwnProperty('events')) {
            for (let key in options.events) {
                el.addEventListener(key, options.events[key])
            }
        }
    }

    if (childrens) {
        if (Array.isArray(childrens)) {
            childrens.map(child => {
                appendChildren(child)
            })
        } else {
            console.warn('Childrens must be an array!');
        }
    }

    function appendChildren(child) {
        if (typeof child === 'string') {
            el.innerHTML = child;
        } else {
            el.appendChild(child);
        }
    }

    return el;
}

export function ajax(data, url) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.onreadystatechange = function (e) {
            if (this.readyState === 4 && this.status === 200) resolve()
            else if(this.status  !== 200) reject()
        }
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(data);

    })

}