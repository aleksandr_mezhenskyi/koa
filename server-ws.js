const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const cors = require('cors');

const app = express();

app.use(express.static('dist'));

app.use(cors())
//initialize a simple http server
const server = http.createServer(app);

//initialize the WebSocket server instance
const wss = new WebSocket.Server({ server });

wss.on('connection', (ws) => {

    //connection is up, let's add a simple simple event
    ws.on('message', (message) => {

        let data = JSON.parse(message);
        let resp = validate(data);
        ws.send(JSON.stringify(resp))
    });
});

app.post('/submit', function(req, res) {
    res.send('hello world');
  });

  app.get('/test', function(req, res) {
    res.send(JSON.stringify({
        page: 'one',
    }));
  });

//start our server
server.listen(process.env.PORT || 8999, () => {
    console.log(`Server started on port ${server.address().port} :)`);
});



function validate(data) {
    let status = true,
        error = {};

    for (let key in data) {
        switch (key) {

            case 'name':
                status = data.name.length < 5;
                error.name = status ? false :  'Введите меньше 5 символов';
                break;

            case 'email':
                status = /[@]/g.test(data.email);
                error.email = status ? false : 'Введите валидный email';
                break;

            case 'phone':
                status = /([0-9])/g.test(data.phone);
                error.phone = status ? false :'Введите только цифры';
                break;

            default:
                break;
        }
    }

    return error;

}