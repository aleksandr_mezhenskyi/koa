const db = require('../mongo')()
const ArticleModel = db.model('Article')

exports.create = async (ctx, next) => {
    const article = new ArticleModel(ctx.request.body)

    try {
        ctx.body = await article.save()
        ctx.status = 201
    } catch (e) {
        ctx.status = 500
        ctx.body = e
    }
}

exports.getAll = async (ctx, next) => {
    try {
        ctx.body = await ArticleModel.find()
        ctx.status = 200
    } catch (e) {
        ctx.status = 500
        ctx.body = e
    }
}


exports.getByAlias = async (ctx, next) => {

    try {
        const article = await ArticleModel.findOne({
            alias: ctx.params.alias
        })
        if (article) {
            ctx.status = 200
            ctx.body = article
        } else{
            ctx.status = 404
            ctx.body = 'Article not Found'
        }
    } catch (e) {
        ctx.status = 500
        ctx.body = e
    }
}